package test;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import calculadora.Calculadora;
import calculadora.DivideByZeroException;

public class CalculadoraTest {

	Calculadora calculadora;
	int n1;
	int n2;
	int result;

	@Before
	public void setUp() {
		this.calculadora = new Calculadora();
		this.n1 = 4;
		this.n2 = 2;
	}

	@Test
	public void sumarTest() {
		result = calculadora.sumar(n1, n2);
		assertEquals(6, result);
	}
	
	@Test
	public void restarTest() {
		result = calculadora.restar(n1, n2);
		assertEquals(2, result);
	}
	
	@Test
	public void multiplicarTest() {
		result = calculadora.multiplicar(n1, n2);
		assertEquals(8, result);
	}
	
	@Test
	public void divirTest() {
		try {
			result = calculadora.dividir(n1, n2);
			assertEquals(2, result);
		} catch (DivideByZeroException e) {
			e.printStackTrace();
		}
	}
	
	@Test(expected = DivideByZeroException.class)
	public void dividirByZeroTest() throws DivideByZeroException {
		result = calculadora.dividir(n1, 0);
		fail();
	}

	@After
	public void tearDown() {
		this.calculadora = null;
	}

}
