package calculadora;

public class Calculadora {
	public int sumar(int operando1, int operando2) {
		return operando1 + operando2;
	}

	public int restar(int operando1, int operando2) {
		return operando1 - operando2;
	}

	public int multiplicar(int operando1, int operando2) {
		return operando1 * operando2;
	}

	public int dividir(int operando1, int operando2) throws DivideByZeroException {
		if (operando2 == 0)
			throw new DivideByZeroException();
		
		return operando1 / operando2;
	}
}
